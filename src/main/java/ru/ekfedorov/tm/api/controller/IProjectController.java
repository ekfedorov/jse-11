package ru.ekfedorov.tm.api.controller;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectByName();

    void showProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void removeProjectById();

    void updateProjectByIndex();

    void updateProjectById();

}

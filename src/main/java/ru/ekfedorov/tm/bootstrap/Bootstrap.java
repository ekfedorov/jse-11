package ru.ekfedorov.tm.bootstrap;

import ru.ekfedorov.tm.api.controller.ICommandController;
import ru.ekfedorov.tm.api.controller.IProjectController;
import ru.ekfedorov.tm.api.controller.ITaskController;
import ru.ekfedorov.tm.api.repository.ICommandRepository;
import ru.ekfedorov.tm.api.repository.IProjectRepository;
import ru.ekfedorov.tm.api.repository.ITaskRepository;
import ru.ekfedorov.tm.api.service.ICommandService;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.api.service.ITaskService;
import ru.ekfedorov.tm.constant.ArgumentConst;
import ru.ekfedorov.tm.constant.TerminalConst;
import ru.ekfedorov.tm.controller.CommandController;
import ru.ekfedorov.tm.controller.ProjectController;
import ru.ekfedorov.tm.controller.TaskController;
import ru.ekfedorov.tm.repository.CommandRepository;
import ru.ekfedorov.tm.repository.ProjectRepository;
import ru.ekfedorov.tm.repository.TaskRepository;
import ru.ekfedorov.tm.service.CommandService;
import ru.ekfedorov.tm.service.ProjectService;
import ru.ekfedorov.tm.service.TaskService;
import ru.ekfedorov.tm.util.TerminalUtil;


public class Bootstrap {

    public final ICommandRepository commandRepository = new CommandRepository();

    public final ICommandService commandService = new CommandService(commandRepository);

    public final ICommandController commandController = new CommandController(commandService);

    public final ITaskRepository taskRepository = new TaskRepository();

    public final ITaskService taskService = new TaskService(taskRepository);

    public final ITaskController taskController = new TaskController(taskService);

    public final IProjectRepository projectRepository = new ProjectRepository();

    public final IProjectService projectService = new ProjectService(projectRepository);

    public final IProjectController projectController = new ProjectController(projectService);

    public void run(final String... args) {
        displayWelcome();
        if (parseArgs(args)) commandController.exit();
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            System.out.println();
            parseCommand(command);
        }
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default: showIncorrectArgument();
        }
    }

    private void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            default: showIncorrectCommand();
        }
    }

    private void showIncorrectCommand() {
        System.out.println("Error! Command not found...\n");
    }

    private void showIncorrectArgument() {
        System.out.println("Error! Argument not found...");
    }

    private boolean parseArgs(final String... args) {
        if (args == null || args.length == 0) return false;
        final String param = args[0];
        parseArg(param);
        return true;
    }

}

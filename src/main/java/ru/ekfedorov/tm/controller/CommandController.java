package ru.ekfedorov.tm.controller;

import ru.ekfedorov.tm.api.controller.ICommandController;
import ru.ekfedorov.tm.api.service.ICommandService;
import ru.ekfedorov.tm.model.Command;
import ru.ekfedorov.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command);
        System.out.println();
    }

    @Override
    public void displayVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.6.0");
        System.out.println();
    }

    @Override
    public void displayAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Evgeniy Fedorov");
        System.out.println("ekfedorov@tsconsulting.com");
        System.out.println();
    }

    @Override
    public void exit() {
        System.exit(0);
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        System.out.println("[COMMANDS]");
        for (final Command command: commands) {
            final String name = command.getName();
            if (name != null) System.out.println(name);
        }
        System.out.println();
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        System.out.println("[ARGUMENTS]");
        for (final Command command: commands) {
            final String arg = command.getArg();
            if (arg != null) System.out.println(arg);
        }
        System.out.println();
    }

    @Override
    public void showSystemInfo() {
        System.out.println("[SYSTEM INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        final String processorMsg = "Available processors: " + processors;
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final String freeMemoryMsg = "Free memory: " + NumberUtil.format(freeMemory);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.format(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final String maxMemoryMsg = "Maximum memory: " + maxMemoryValue;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final String totalMemoryMsg = "Total memory available to JVM: " + NumberUtil.format(totalMemory);
        final long usedMemory = totalMemory - freeMemory;
        final String usedMemoryMsg = "Used memory by JVM: " + NumberUtil.format(usedMemory);
        System.out.println(processorMsg);
        System.out.println(freeMemoryMsg);
        System.out.println(maxMemoryMsg);
        System.out.println(totalMemoryMsg);
        System.out.println(usedMemoryMsg);
        System.out.println();
    }

}

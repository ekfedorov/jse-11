package ru.ekfedorov.tm.constant;

public interface TerminalConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_COMMANDS = "commands";

    String CMD_ARGUMENTS = "arguments";

    String CMD_TASK_CREATE = "task-create";

    String CMD_TASK_CLEAR = "task-clear";

    String CMD_TASK_LIST = "task-list";

    String CMD_PROJECT_CREATE = "project-create";

    String CMD_PROJECT_CLEAR = "project-clear";

    String CMD_PROJECT_LIST = "project-list";

    String CMD_TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String CMD_TASK_UPDATE_BY_ID = "task-update-by-id";

    String CMD_TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String CMD_TASK_REMOVE_BY_ID = "task-remove-by-id";

    String CMD_TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String CMD_TASK_VIEW_BY_INDEX = "task-view-by-index";

    String CMD_TASK_VIEW_BY_NAME = "task-view-by-name";

    String CMD_TASK_VIEW_BY_ID = "task-view-by-id";

    String CMD_PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String CMD_PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String CMD_PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String CMD_PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String CMD_PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String CMD_PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String CMD_PROJECT_VIEW_BY_NAME = "project-view-by-name";

    String CMD_PROJECT_VIEW_BY_ID = "project-view-by-id";

}
